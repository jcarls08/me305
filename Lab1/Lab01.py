# -*- coding: utf-8 -*-
"""
@file Lab01.py

@brief A file which calculates and outputs the indexed Fibonacci number
@details Program asks for a positive integer from the user which is used as the index to find the associated Fibonacci number


@author: Julia Carlson
""" 

def fib (idx) :
    '''This method calculates a Fibonacci number corresponding to a specified index. 
    @param idx An integer specifying the index of the desired Fibonacci number.'''

#Define first two values of Fib Seq    
    idx_0 = 0
    idx_1 = 1
    if type(idx) is not int :
        print("Please type an int ")
        #Doesn't allow negative inputs
    elif idx < 0:
        print("Negative input not defined for Fib Seq")
        #Prints zero for zero index
    elif idx == 0:
        print(idx_0)
        #Prints one for one index
    elif idx == 1:
        print(idx_1)
        #Calculates fib seq for index greater than 1
    else:
        for idx in range(2,idx+1):
            idx_2 = idx_0 + idx_1
            idx_0 = idx_1
            idx_1 = idx_2
        print(idx_1)
        
#Prompts user to input index continuously
if __name__ == '__main__':
    a = 1
   #Returns error for non-integers
    try:
        idx = int(input('Please input index: '))
        fib(idx)
    except:
        print('Please type an integer')
    while a==1:
        idx = int(input('Please input index: '))
        fib(idx)