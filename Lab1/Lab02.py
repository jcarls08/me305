# -*- coding: utf-8 -*-
"""
@file Lab02.py

@brief This file defines a FSM for two LEDs that blinks

@author: Julia Carlson
"""

import time
import pyb
import utime

class TaskBlinkVirtual:
    ## Constant defining state 0
    S0_INIT   = 0
    ## Constant defining state 1
    S1_LED_on  = 1
    ## Constant defining state 2
    S2_LED_off = 2

    def __init__(self, interval):
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e6)
        
        ## The timestamp for the first iteration
        #self.start_time = time.time()
        ## NUCLEO TIME
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        #self.next_time = self.start_time + self.interval
        ## NUCLEO TIME
        self.next_time = utime.ticks_add(self.start_time, self.interval)

    def run(self):
        ## set current time
        #self.curr_time = time.time()
        ## NUCLEO TIME
        self.curr_time = utime.ticks_us()
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            
            # Always transtion from state 0 to state 1
            if(self.state == self.S0_INIT):
                print('Initialization')
                # Turn on virtual LED
                self.transitionTo(self.S1_LED_on)
             
            # If the virtual LED is on, Turn virtual LED off    
            elif(self.state == self.S1_LED_on):
                self.transitionTo(self.S2_LED_off)
                print('Virtual LED Off')
                
            # If the virtual LED is off, Turn virtual LED on     
            elif(self.state == self.S2_LED_off):
                self.transitionTo(self.S1_LED_on)
                print('Virtual LED On')
                
            else:
                # Invalid state code (error handling)
                pass
            
            # Increment runs up
            self.runs += 1
            
            # Specifying the next time the task will run
            #self.next_time = self.next_time + self.interval
            ## NUCLEO TIME
            self.next_time = utime.ticks_add(self.start_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
    
                
                
class TaskBlinkPhysical:
    ## Constants defining state 0
    S0_INIT   = 0
    ## Constants defining state 1
    S1_LED_pattern  = 1
    
    def __init__(self, interval, VAL):
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The amount of time in seconds between runs of the task
        self.interval = int(interval*1e6)
       
        ## start Value at 0
        self.VAL = 0
        
        #self.start_time = time.time()
        ## NUCLEO TIME
        self.start_time = utime.ticks_us()
        
        #self.next_time = self.start_time + self.interval
        ## NUCLEO TIME
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        self.VAL_max = 100
        
   
    def run(self):
        pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        tim2 = pyb.Timer(2, freq = 20000)
        t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
        #self.curr_time = time.time()
        self.curr_time = utime.ticks_us()
        
        # Begins running if the start time has past
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            
            # Always transtion from state 0 to state 1
            if(self.state == self.S0_INIT):
                #print('State 0')
                t2ch1.pulse_width_percent(self.VAL)
                self.transitionTo(self.S1_LED_pattern)
                
            # If on state 1, check the Value
            elif(self.state == self.S1_LED_pattern):
                #print('State 1')
               # self.VAL += 10
                t2ch1.pulse_width_percent(self.VAL)
                # If value is less than max, add 1 to value 
                if(self.VAL <= self.VAL_max and (self.runs % 10)==0):
                    self.VAL += 1
                    #print('LED brighter')
                    t2ch1.pulse_width_percent(self.VAL)
                
                # If value is more than max, set value to 0
                elif(self.VAL >= self.VAL_max and (self.runs % 10)==0):
                    self.VAL = 0
                    #print('LED off')
                    t2ch1.pulse_width_percent(0)
        else:
                # Invalid state code (error handling)
                pass
            
                self.runs += 10
            
            # Specifying the next time the task will run
                self.next_time = utime.ticks_add(self.start_time, self.interval)
                
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState