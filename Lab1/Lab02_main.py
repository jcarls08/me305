# -*- coding: utf-8 -*-
"""
@file Lab02.py

@brief This file defines a FSM for two LEDs that blinks

@author: Julia Carlson
"""

from Lab02 import TaskBlinkPhysical, TaskBlinkVirtual

import pyb
# import LED port instructions
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
t2ch1.pulse_width_percent()

#Run Task 1 (Virtual LED)
task1 = TaskBlinkVirtual(10)
#Run Task 2 (Physical LED)
task2 = TaskBlinkPhysical(10,100)
for n in range(1000000):
    task1.run()
    task2.run()