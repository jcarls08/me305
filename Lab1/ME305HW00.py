'''
@file FSM_example.py

This file is a finite-state-machine for an elevator oscillating between floor 
1 and 2. 

There are two buttons outside of the elevator (one on each floor), 
for floor 1 and one for floor 2.
There are two more buttons inside the elevator for the same purpose.

There is also a sensor inside the elevator to recognize when the specific
floor has been reached.
'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control an elevator.
    @details    This class implements a finite state machine to control the
                operation of an elevator travelling between floor 1 & 2.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT = 0    
    
    ## Constant defining State 1
    S1_MOVING_DOWN = 1    
    
    ## Constant defining State 2
    S2_MOVING_UP = 2    
    
    ## Constant defining State 3
    S3_STOPPED_FLOOR_1 = 3    
    
    ## Constant defining State 4
    S4_STOPPED_FLOOR_2  = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING  = 5
    
    def __init__(self, interval, BUTTON_1, BUTTON_2, FIRST, SECOND, Motor):
        '''
        @brief            Creates a TaskElevator object.
        @param BUTTON_1   An object from class Button representing Floor 1 transition
        @param BUTTON_2   An object from class Button representing Floor 2 transition
        @param FLOOR_1    An object from class Button representing the bottom limit
        @param FLOOR_2    An object from class Button representing the upper limit
        @param Motor      An object from class MotorDriver representing a DC motor.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The button object used for floor 1
        self.BUTTON_1 = BUTTON_1
        
        ## The button object used for floor 2
        self.BUTTON_2 = BUTTON_2
        
        ## The button object used for the bottom limit
        self.FIRST = FIRST
        
        ## The button object used for the upper limit
        self.SECOND = SECOND
        
        ## The motor object making the elevator move
        self.Motor = Motor
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
        if (self.curr_time > self.next_time):
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Motor.DOWN()
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S3_STOPPED_FLOOR_1):
                # Run State 3 Code
                
                if(self.runs*self.interval > 10):  # if our tsk runs for 10 seconds, transition
                    self.transitionTo(self.S5_DO_NOTHING)
                    
                if(self.BUTTON_2.getButtonState()):
                    self.transitionTo(self.S2_MOVING_UP)
                    self.Motor.UP()
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S4_STOPPED_FLOOR_2):
                # Run State 4 Code
                if(self.runs*self.interval > 10):  # if our tsk runs for 10 seconds, transition
                    self.transitionTo(self.S5_DO_NOTHING)
                    
                if(self.BUTTON_1.getButtonState()):
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor.DOWN()
                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S1_MOVING_DOWN):
                # Transition to state 3 if first=1
                if(self.FIRST.getButtonState()):
                    self.transitionTo(self.S3_STOPPED_FLOOR_1)
                    self.Motor.Stop()
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
                
            elif(self.state == self.S2_MOVING_UP):
                # Transition to state 4 if SECOND=1
                if(self.SECOND.getButtonState()):
                    self.transitionTo(self.S4_STOPPED_FLOOR_2)
                    self.Motor.Stop()
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
            
#            elif(self.state == self.S2_MOVING_UP):
#                # Run State 2 Code
#                if(self.SECOND.getBUTTON_2State()):
#                    self.transitionTo(self.S4_STOPPED.FLOOR_2)
#                    self.Motor.Stop()
#                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
                
            elif(self.state == self.S5_DO_NOTHING):
                # Run state 5 code
                print(str(self.runs) + ': State 5 ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the elevator
    go up and down
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def DOWN(self):
        '''
        @brief Moves the motor UP
        '''
        print('Motor moving UP')
    
    def UP(self):
        '''
        @brief Moves the motor DOWN
        '''
        print('Motor moving DOWN')
    
    def Stop(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor Stopped')



























