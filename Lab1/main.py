# -*- coding: utf-8 -*-
"""
@file main.py

@author: Julia Carlson
"""
import shares
import pyb
from TaskUser import TaskUser
from TaskEncoder import TaskEncoder
from TaskEncoder import EncoderDriver

## User interface task, works with serial port
task0 = TaskUser  (0, 10_000, dbg=False)

## Enocder task returns the postion of the motor at a given time
enc = EncoderDriver(0xFFFF, pyb.Pin.cpu.A6, pyb.Pin.cpu.A7, 3)
EncoderDriver.tim.init(0,EncoderDriver.period)
EncoderDriver.tim.channel(1, pin=pyb.Pin.cpu.A6, mode=pyb.Timer.ENC_AB)
EncoderDriver.tim.channel(2, pin=pyb.Pin.cpu.A7, mode=pyb.Timer.ENC_AB)

task1 = TaskEncoder(enc, 10_000, dbg=False)

## The task list contains the tasks to be run "round-robin" style
taskList = [task0,
            task1]

while True:
    for task in taskList:
        task.run()