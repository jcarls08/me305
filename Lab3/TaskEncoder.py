# -*- coding: utf-8 -*-
"""
@file TaskEncoder.py

@brief

"""

import shares
import utime
import pyb

class TaskEncoder:
    ## Defining State 0- Intialization
    S0_INIT = 0
    ## Constant defining state 1
    S1_return_position = 1

    
    
    def __init__(self, encoder, taskNum, interval, dbg=True):
        
        ## The encoder object used to find the position
        self.encoder = encoder
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## NUCLEO start time
        self.start_time = utime.ticks_us()
        
        ## NUCLEO incrementing time
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
        
        ## The number of the task
        self.taskNum = taskNum
        
        if self.dbg:
            print('Created user encoder task')
        
   
    def run(self):
       '''
       Runs one iteration of the task
       '''
        
       ## Setting up timer and encoder from Nucleo
       tim = pyb.Timer(3)
       tim.init(prescaler=0, period=0xFFFF)
       tim.channel(1, pin=pyb.Pin.cpu.A6, mode=pyb.Timer.ENC_AB)
       tim.channel(2, pin=pyb.Pin.cpu.A7, mode=pyb.Timer.ENC_AB)
       
       
       ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
       self.curr_time = utime.ticks_us()
       if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            
           # Always transtion from state 0 to state 1
            if(self.state == self.S0_INIT):
                #self.printTrace()
                # Return updated position
                self.transitionTo(self.S1_return_position)
             
            # If zero command given, zero the position
            elif(self.state == self.S1_return_position):
                if (shares.cmd == 'z'):
                    shares.resp = self.encoder(shares.cmd)
                    shares.cmd = None
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
        
        
class EncoderDriver:
    def __init__(self, period, pin1, pin2, tim):
        ## The period of the encoder
        self.period = period
        ## Create the pin object
        self.pin1 = pin1
        ## Create the pin object
        self.pin2 = pin2
        ## Create the timer object
        self.tim = tim
    
    def update(self, delta, position):
        ## read timer
        self.timer
        
        self.position = shares.cmd
        
        ## subtract old count to get delta
        self.delta = self.positon - self.last_count
        
        ##"fix" delta and add to position
        ## is the mag of the delta greater than half the period?
        if (abs(self.delta) >= (self.period*0.5)):
            ##if delta is negative, add the peirod to 'fix' the delta 
            if self.delta < 0:
                self.position = self.position + self.period
            ##if delta is positive, subtract the period to 'fix' the delta
            elif self.delta > 0:
                self.position = self.position - self.period
   
    ## Define the last count as the current position and the the position as the user command
    def set_position(self, cmd):
        #takes value from the user
        self.last_count = self.position
        self.position = cmd
        
    ## Return the current position
    def get_position(self):
        return self.position

    ## Return the delta value
    def get_delta(self):
        return self.delta

        