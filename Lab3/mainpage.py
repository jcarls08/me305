## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  ME 305 Labs Fall 2020
#
#  
#  @page page_Lab01 Lab 01 Fibonacci
#  Fibonacci Program, info about lab01.Lab01
#  @section Source Code Link
#  https://bitbucket.org/jcarls08/me305/src/master/Lab1/Lab01.py
#
#  @author Julia Carlson
#
#  @date September 23, 2020
#
#
#  @page page_hw_0 Homework 00 FSM Elevator
#  @section Source Code Link
#  https://bitbucket.org/jcarls08/me305/src/master/Lab1/ME305HW00.py
#  @image html hw00statediagram.png
#
#  @author Julia Carlson
#
#  @date October 7, 2020
#
#
#  @page page_Lab02 Blinking LED FSM
#  Blinking LEDs, info about lab02.Lab02.
#  @section Source Code Links
#  Full code file:
#  https://bitbucket.org/jcarls08/me305/src/master/Lab1/Lab02.py
#  Main File:
#  https://bitbucket.org/jcarls08/me305/src/master/Lab1/Lab02_main.py
#  @image html Lab02FSM.png
#
#  @author Julia Carlson
#
#
#  @date October 14, 2020
#
#  @page page_Lab03 Motor Encoder
#  Motor Encoder, info about lab03.Lab03.
#  @section Source Code Links
#  TaskEncoder code file:
#  https://bitbucket.org/jcarls08/me305/src/master/Lab3/TaskEncoder.py
#  TaskUser code file:
#  https://bitbucket.org/jcarls08/me305/src/master/Lab3/TaskUser.py
#  shares code file:
#  https://bitbucket.org/jcarls08/me305/src/master/Lab3/shares.py
#  Main File:
#  https://bitbucket.org/jcarls08/me305/src/master/Lab1/Lab02_main.py
#  @image html Lab3Transition.png
#  @image html Lab3Task.png
#
#  @author Julia Carlson
#
#
#  @date October 14, 2020