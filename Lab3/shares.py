'''
@file shares.py
@brief A container for all the inter-task variables
@author Julia Carlson
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

## The command character sent from the user interface task to the encoder task
cmd     = None

## The response from the encoder task after encoding
resp    = None